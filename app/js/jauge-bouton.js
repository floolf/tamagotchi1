const elemSom = document.getElementById("sommeil");
const elemFaim = document.getElementById("faim");
const elemHyg = document.getElementById("hygiene");

let widthSommeil = 100;
let widthFaim = 100;
let widthHygiene = 100;

let idSommeil;
let idFaim;
let idHygiene;

const btnPlay = document.getElementById('play');


//PARTI SOMMEIL
function jaugeSommeil() {
    idSommeil = setInterval(frame, 2000);
    function frame() {
        if (widthSommeil <= 0) {
            alert('Perdu');
            clearInterval(idSommeil);
        } else {
            widthSommeil--;
            elemSom.style.width = widthSommeil + '%';
        }
    }
}

//bouton
function dormir() {
    widthSommeil = 100;
    clearInterval(widthSommeil);
    elemSom.style.width = widthSommeil + '%';
}

//PARTI FAIM
function jaugeFaim() {
    idFaim = setInterval(frame, 200);
    function frame() {
        if (widthFaim <= 0) {
            alert('Perdu');
            clearInterval(idFaim);
        } else {
            widthFaim--;
            elemFaim.style.width = widthFaim + '%';
        }
    }
}

//bouton
function manger() {
    widthFaim = 100;
    clearInterval(widthFaim);
    elemFaim.style.width = widthFaim + '%';
}

//PARTI HYGIENE
function jaugeHygiene() {
    idHygiene = setInterval(frame, 1300);
    function frame() {
        if (widthHygiene <= 0) {
            alert('Perdu');
            clearInterval(idHygiene);
        } else {
            widthHygiene--;
            elemHyg.style.width = widthHygiene + '%';
        }
    }
}

//bouton
function changer() {
    widthHygiene = 100;
    clearInterval(widthHygiene);
    elemHyg.style.width = widthHygiene + '%';
}


btnPlay.onclick = function play() {
    jaugeSommeil();
    jaugeFaim();
    jaugeHygiene();
};